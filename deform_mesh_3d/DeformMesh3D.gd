@tool
extends MeshInstance3D
@export var curve : Curve3D
@export var mesh_axis = Vector3(0, 1, 0)
@export var original_mesh : Mesh

func _init():
	
	var working_mesh = ArrayMesh.new()
	var surface_count = original_mesh.get_surface_count()
	var line_size = curve.get_baked_points().size()
	var curve_points = curve.get_baked_points()
	var prim_axis = mesh_axis.max_axis() # Returns an int for the AXIS_ constants,  will use this to order vecs into a xform after householder reflections

	var mdt = MeshDataTool.new()
	
	print("surface count: " + str(surface_count))
	for surface_id in surface_count:
		print("in surface " + str(surface_id))
		var arrays = original_mesh.surface_get_arrays(surface_id)
		working_mesh = ArrayMesh.new()
		var ref_mesh = ArrayMesh.new()
		ref_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
		mdt.create_from_surface(ref_mesh, surface_id)
		print("mdt: " + str(mdt))
		var vertex_count = mdt.get_vertex_count()
		print("vertex count: " + str(vertex_count))
		#Do a prepass to count y component, which we'll use as max height 
		#to divide points by so we know where each vertex is on the curve 
		var max_y = 0.0
		var min_y = null
		for i in vertex_count:
			var vertex = mdt.get_vertex(i)
			if vertex.y > max_y:
				max_y = vertex.y
			if min_y == null or vertex.y < min_y:
				min_y = vertex.y
		print("max_y: " + str(max_y))
		
		var current_line_pos = null
		for i in vertex_count:
			var vertex = mdt.get_vertex(i)
			var pc_along_line = ((vertex.y + abs(min_y)) / (abs(min_y) + max_y)) # Throwing this abs(min_y) thing in so if the mesh is centered on (0,0,0), if we have any negative Y vertices, we just shunt the entire thing up by the largest negative amount and operate entirely on positive space
			var line_pos = int(pc_along_line * (line_size - 1))
			if line_pos != current_line_pos:
				print("line_pos: " + str(line_pos))
				current_line_pos = line_pos
			var point_pos = curve_points[line_pos]
			var line
			if line_pos > 0:
				line = point_pos - curve_points[line_pos - 1] as Vector3
			else:
				line = curve_points[line_pos + 1] - point_pos as Vector3
			line = line.normalized() as Vector3
			
			var vx_xform
			if line.is_equal_approx(mesh_axis):
				vx_xform = Transform3D(Basis.IDENTITY, point_pos)
			else:
				var l = line # Will replace the mesh's old primary axis
				var u = mesh_axis.normalized() # Should be one of the cardinal axes
				var v = l.cross(u).normalized() # Gives a vec orthogonol to LINE/MAIN_AXIS plane, giving us 2 of 3 basis axes
				var w = v.cross(l) # Vec orthogonal to V/LINE plane, replaces 
				vx_xform = Transform3D(-v, -l, -w, point_pos) 

			
			mdt.set_vertex(i, vx_xform * vertex)
		
		mdt.commit_to_surface(working_mesh)
		var st = SurfaceTool.new()
		st.begin(Mesh.PRIMITIVE_TRIANGLES)
		st.create_from(working_mesh, surface_id)
		st.generate_normals()
		st.commit(working_mesh)
	mesh = null
	mesh = working_mesh
	
	$GIProbe.bake()
		
