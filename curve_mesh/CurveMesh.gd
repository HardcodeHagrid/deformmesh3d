@tool
extends MeshInstance3D

@export var curve : Curve3D
@export var radius = 0.2
@export var resolution = 4 
@export var autobuild = false
var color = Color.RED

func build():
	var curve_points = curve.get_baked_points()
	var curve_length = curve_points.size()
	var current_point  = Vector3(curve_points[1])
	var prev_point = Vector3(curve_points[0])
	var line = Vector3(current_point - prev_point)
	var angles = 2 * PI / resolution 
	
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	
	var u = line.normalized()
	var v = Vector3(0,1,0)
	var w = u.cross(v).normalized()
	v = w.cross(u)
	
	var xform = Transform3D(v, u, w, prev_point)
	st.add_vertex(prev_point)
	for i in range(resolution):
		var vec = Vector3(cos(i * angles) * radius, 0, sin(i * angles) * radius)
		st.add_vertex(xform * vec)
	for i in range(resolution - 1):
		st.add_index(0)
		st.add_index(1 + i)
		st.add_index(2 + i)
	#Last tri on initial face uses the first non-centred vertex again, so loop one short and implement manually 
	st.add_index(0)
	st.add_index(resolution)
	st.add_index(1)
	for n in range(1, curve_length):
		current_point = curve_points[n]
		prev_point = curve_points[n - 1]
		line = Vector3(current_point - prev_point).normalized()

		u = line.normalized()
		v = Vector3(0,1,0)
		w = u.cross(v).normalized()
		v = w.cross(u)
		if line.is_equal_approx(Vector3(0, 1, 0)):
			xform = Transform3D(Basis.IDENTITY, current_point)
		else:			
			xform = Transform3D(v, u, w, current_point)
		
		for i in range(resolution):
			var vec = Vector3(cos(i * angles) * radius, 0, -sin(i * angles) * radius)
			st.add_vertex(xform * vec)
		
		for i in range(1, resolution):
			st.add_index(n * resolution  + i)
			st.add_index((n - 1) * resolution + i + 1)
			st.add_index((n - 1) * resolution + i)
			
			st.add_index(n * resolution + i)
			st.add_index(n * resolution + i + 1)
			st.add_index((n - 1) * resolution + i + 1)
		st.add_index(n * resolution + resolution)
		st.add_index((n - 1) * resolution + 1)
		st.add_index((n - 1) * resolution + resolution)
		

		st.add_index(n * resolution + resolution)
		st.add_index((n - 1) * resolution + resolution + 1)
		st.add_index((n - 1) * resolution + 1)
	st.generate_normals()
	
	var mat = StandardMaterial3D.new()
	mat.albedo_color = Color.BLUE
	#mat.emission = Color.skyblue
	mesh = st.commit(ArrayMesh.new())
	mesh.surface_set_material(0, mat)

func _ready():
	if autobuild:
		build()
