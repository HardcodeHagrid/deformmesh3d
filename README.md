# DeformMesh3D

Basic GDScript mesh collision along curve
The DeformMesh3D scene is the demo scene rn, most useful will be the curvemesh. If you are using DeformMesh, it needs an original mesh first.

To us curvemesh, put in a curve, (you don't need to put in an existing mesh as one will be made), and call its build function. There's an 
autobuild option for if you want this to happen on curvemesh's _ready func

material is set in the code at the end of build()

They're set as tools by default, you may want to change this for use in-project. Likewise one side of the curve is left open just to save faces for now
but the other is joined up and you can just duplicate this in the code if you need to. It's as it is now due to time constraints & it fitting my needs atm
